// Turtle Bibliothek von http://www.aplu.ch/
// Javadoc unter http://www.aplu.ch/classdoc/turtle/index.html
import ch.aplu.turtle.*;

// import galapagos.*;
/**
 * Beschreiben Sie hier die Klasse TurtleDraw.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class TurtleDraw
{
    Turtle t = new Turtle();
    
    public  TurtleDraw(){
       // t.clear();
    }
    
    public void quadrat(int l) {
       
        for(int i=0; i<4; i++) {
            t.forward(l);
            t.left(90);
        }
        
    }
    
    public void hausVomNikolaus() {
        
    }
    
    
    public void clearPlayground() {
        t.clear();
        t.st();
    }
    
    public void setTurtleToOrigin() {
        t.setPos(0,0);
        t.heading(90);
    }
    
     public void setTurtleToLeft() {
        t.setPos(-180,0);
        t.heading(90);
    }
   
}
